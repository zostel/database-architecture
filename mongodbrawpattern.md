# DataBase Structure For MongoDB

## Booking (Collection)

pcode (Property Code)	            -   (Id) Reference to Property Model
cusid (Customer Id)					-	(Id) Reference to Customer Model
date (Date of booking)			    -	Date
name (Name on Booking)				-	Text
nguests (Number of guest)			-	number
cin (Check-In Date)					-	Date
cout (Check Out Date)				- 	Date
arrival (Proposed Arrival Time)		-	Time
price (Price)						-	number
bookvia (Booking Via)				-	text
note (Comments)					    -	text
rtypes (Room Type Code)				-	object (Map of rtype: nguests)
paid (Advance Paid)					-	number
cost (Total Cost)				    -   number


# Customer (Collection)
name (Name of customer)				-	text
dob (Date of Birth)					-	date
sex (Gender)						-	text
addr (Address)						-	text
email (E-Mail)						-	text
phone (Phone Number)				-	number
history (Booking History)			-	Array of  Booking ids
pics (Pictures)						-	Array of Image Links
socialuids (Facebook username)		-	Object (Map of socialsite: username)
nationality (Nationality)			-	text

#ZProperty (Collection)
rtypes						-	{“rtype1”: [occupied, total]}
name						- 	text
paddr						-	text
phones						-	{‘mobile’: , ‘landline’: }
facilities					- 	Array of facilities (‘ac’,’wifi’,’restaurant’}
city						-	text
pin	                        -   number

