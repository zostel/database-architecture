## Booking Management Data:
1.       Date of Booking
2.       Name of Booking
3.       Number of Guests
4.       Number of Nights
5.       Date of Arrival
6.       Email Address
7.       Contact info
8.       Nationality
9.       Price
10.   Booking Via: Online website, Hostelworld, Facebook, Twitter, booking.com, etc.

## Revenue Management Data
1.       Booking Revenue on a daily basis
2.       Other sources of Revenue

## Expense Management
1.       Daily expenses
2.       Electricity bills
3.       Salary expenses
4.       Foods and Drinks

## Customer Management
1.       Name
2.       Age
3.       Sex
4.       Email
5.       Phone
6.       Nights stayed
7.       Revenue contributed
8.       Picture of all customers
9.       Feedback received
10.      Reviews Contributed

## Property Presence
1.       Is Property present on platforms
2.       What is current rating?
3.       What is the current position?
4.       How many bookings from each platform

## Dynamic Pricing
1.       Flexibility to change price at any moment of time
2.       If possible reflect it on all platforms
